#include "first_app.hpp"

// Standard Libraries
#include <stdexcept>
#include <array>

namespace rve
{
  
// Constructor
FirstApp::FirstApp()
{
  createPipelineLayout();
  createPipeline();
  createCommandBuffers();
} // Constructor ^

// Destructor
FirstApp::~FirstApp()
{
  vkDestroyPipelineLayout(rveDevice.device(), pipelineLayout, nullptr);
} // Destructor ^

// Run
void FirstApp::run()
{
  while (!rveWindow.shouldClose())
  {
    glfwPollEvents();
    drawFrame();
  }
} // Run ^

// Create Pipeline Layout
void FirstApp::createPipelineLayout()
{
  VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
  pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
  pipelineLayoutInfo.setLayoutCount = 0;
  pipelineLayoutInfo.pSetLayouts = nullptr;
  pipelineLayoutInfo.pushConstantRangeCount = 0;
  pipelineLayoutInfo.pPushConstantRanges = nullptr;
  
  if (vkCreatePipelineLayout(rveDevice.device(), &pipelineLayoutInfo, nullptr, &pipelineLayout) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to create pipeline layout!");
  }
} // Create Pipeline Layout ^

// Create Pipeline
void FirstApp::createPipeline()
{
  PipelineConfigInfo pipelineConfig{};
  RvePipeline::defaultPipelineConfigInfo(pipelineConfig, WIDTH, HEIGHT);
  pipelineConfig.renderPass = rveSwapChain.getRenderPass();
  pipelineConfig.pipelineLayout = pipelineLayout;
  rvePipeline = std::make_unique<RvePipeline>(
    rveDevice,
    "shaders/simple_shader.vert.spv",
    "shaders/simple_shader.frag.spv",
    pipelineConfig);
} // Create Pipeline ^

// Create Command Buffers
void FirstApp::createCommandBuffers()
{
  commandBuffers.resize(rveSwapChain.imageCount());
  
  VkCommandBufferAllocateInfo allocInfo{};
  allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  allocInfo.commandPool = rveDevice.getCommandPool();
  allocInfo.commandBufferCount = static_cast<uint32_t>(commandBuffers.size());
  
  if (vkAllocateCommandBuffers(rveDevice.device(), &allocInfo, commandBuffers.data()) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to allocate command buffers!");
  }
  
  for (int i = 0; i < commandBuffers.size(); i++)
  {
    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    
    if (vkBeginCommandBuffer(commandBuffers[i], &beginInfo) != VK_SUCCESS)
    {
      throw std::runtime_error("failed to begin recording command buffer!");
    }
    
    VkRenderPassBeginInfo renderPassInfo{};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassInfo.renderPass = rveSwapChain.getRenderPass();
    renderPassInfo.framebuffer = rveSwapChain.getFrameBuffer(i);
    
    renderPassInfo.renderArea.offset = {0, 0};
    renderPassInfo.renderArea.extent = rveSwapChain.getSwapChainExtent();
    
    std::array<VkClearValue, 2> clearValues{};
    clearValues[0].color = {0.1f, 0.1f, 0.1f, 1.0f};
    clearValues[1].depthStencil = {1.0f, 0};
    renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
    renderPassInfo.pClearValues = clearValues.data();
    
    vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
    
    rvePipeline->bind(commandBuffers[i]);
    vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);
    
    vkCmdEndRenderPass(commandBuffers[i]);
    if (vkEndCommandBuffer(commandBuffers[i]) != VK_SUCCESS)
    {
      throw std::runtime_error("failed to record command buffer!");
    }
  }
} // Create Command Buffers ^

// Draw Frame
void FirstApp::drawFrame()
{
  uint32_t imageIndex;
  auto result = rveSwapChain.acquireNextImage(&imageIndex);
  
  if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
  {
    throw std::runtime_error("failed to acquire swap chain image!");
  }
  
  result = rveSwapChain.submitCommandBuffers(&commandBuffers[imageIndex], &imageIndex);
  if (result != VK_SUCCESS)
  {
    throw std::runtime_error("failed to present swap chain image!");
  }
} // Draw Frame ^


} // namespace rve ^