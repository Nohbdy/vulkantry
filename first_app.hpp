#ifndef FIRST_APP_HPP
#define FIRST_APP_HPP

#include "rve_window.hpp"
#include "rve_pipeline.hpp"
#include "rve_swap_chain.hpp"
#include "rve_device.hpp"

// Standard Libraries
#include <memory>
#include <vector>

namespace rve
{

// First App
class FirstApp
{
	public:
    static constexpr int WIDTH = 800;
    static constexpr int HEIGHT = 600;
    
    FirstApp();
    ~FirstApp();
    
    FirstApp(const FirstApp &) = delete;
    FirstApp &operator=(const FirstApp &) = delete;
    
    void run();
    
	private:
    void createPipelineLayout();
    void createPipeline();
    void createCommandBuffers();
    void drawFrame();
    
    RveWindow rveWindow{WIDTH, HEIGHT, "Hello Vulkan"};
    RveDevice rveDevice{rveWindow};
    RveSwapChain rveSwapChain{rveDevice, rveWindow.getExtent()};
    std::unique_ptr<RvePipeline> rvePipeline;
    VkPipelineLayout pipelineLayout;
    std::vector<VkCommandBuffer> commandBuffers;
}; // First App ^

}

#endif