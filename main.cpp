/// VulkanTry - Ryan McGill - 2021 CC0

#include "rve_window.hpp"
#include "first_app.hpp"

// Standard Libraries
#include <cstdlib>
#include <iostream>
#include <stdexcept>

// Main
int main()
{
  rve::FirstApp app{};
  
  try
  {
    app.run();
  }
  catch (const std::exception &e)
  {
    std::cerr << e.what() << '\n';
    return EXIT_FAILURE;
  }
  
  return EXIT_SUCCESS;
  
} // Main ^



// Previous testing code

//#define GLFW_INCLUDE_VULKAN
//#include <GLFW/glfw3.h>
//
//#define GLM_FORCE_RADIANS
//#define GLM_FORCE_DEPTH_ZERO_TO_ONE
//#include <glm/mat4x4.hpp>
//#include <glm/vec4.hpp>
//#include <iostream>
//
//int main()
//{
//	glfwInit(); // Some GLFW init thingy, todo research on mayhaps
//
//	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API); // Seems.... hinty
//	GLFWwindow* window = glfwCreateWindow(800, 600, "Vulkan window", nullptr, nullptr);
//
//	uint32_t extensionCount = 0;
//	vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
//
//	std::cout << extensionCount << " extensions supported\n";
//
//	glm::mat4 matrix;
//	glm::vec4 vec;
//	auto test = matrix * vec;
//
//	while (!glfwWindowShouldClose(window))
//	{
//		glfwPollEvents();
//	}
//
//	glfwDestroyWindow(window);
//
//	glfwTerminate();
//
//	return 0;
//}

//#include <stdio.h>
//#include <stdlib.h>
//#include <vulkan/vulkan.h>
//
//int main(int argc, char *argv[]) {
//
//    VkInstanceCreateInfo vk_info;
//    VkInstance inst = 0;
//    VkResult res;
//
//    vk_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
//
//    vk_info.pNext = NULL;
//
//    vk_info.pApplicationInfo = NULL;
//
//    vk_info.enabledLayerCount = 0;
//
//    vk_info.ppEnabledLayerNames = NULL;
//
//    vk_info.enabledExtensionCount = 0;
//
//    vk_info.ppEnabledExtensionNames = NULL;
//
//    res = vkCreateInstance(&vk_info, NULL, &inst);
//
//    if (res != VK_SUCCESS) {
//        // Error!
//        printf("Error %d\n", res);
//        return 1;
//    };
//
//    printf("Device created: %p\n", inst);
//
//    vkDestroyInstance(inst, NULL);
//    return (EXIT_SUCCESS);
//}