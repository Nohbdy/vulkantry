#ifndef RVE_DEVICE_H
#define RVE_DEVICE_H

#include "rve_window.hpp"

// Standard Libraries
#include <string>
#include <vector>

namespace rve
{

// Swap Chain Support Details
struct SwapChainSupportDetails
{
  VkSurfaceCapabilitiesKHR capabilities;
  std::vector<VkSurfaceFormatKHR> formats;
  std::vector<VkPresentModeKHR> presentModes;
}; // Swap Chain Support Details ^

// Queue Family Indices
struct QueueFamilyIndices
{
  uint32_t graphicsFamily;
  uint32_t presentFamily;
  bool graphicsFamilyHasValue = false;
  bool presentFamilyHasValue = false;
  bool isComplete()
  {
    return graphicsFamilyHasValue && presentFamilyHasValue;
  }
}; // Queue Family Indices ^

// RVE Device
class RveDevice
{
  public:
#ifdef NDEBUG
    const bool enableValidationLayers = false;
#else
    const bool enableValidationLayers = true;
#endif

    RveDevice(RveWindow &window);
    ~RveDevice();

    // Not copyable or movable
    RveDevice(const RveDevice &) = delete;
    void operator=(const RveDevice &) = delete;
    RveDevice(RveDevice &&) = delete;
    RveDevice &operator=(RveDevice &&) = delete;

    VkCommandPool getCommandPool()
    {
      return commandPool;
    }
    VkDevice device()
    {
      return device_;
    }
    VkSurfaceKHR surface()
    {
      return surface_;
    }
    VkQueue graphicsQueue()
    {
      return graphicsQueue_;
    }
    VkQueue presentQueue()
    {
      return presentQueue_;
    }

    SwapChainSupportDetails getSwapChainSupport()
    {
      return querySwapChainSupport(physicalDevice);
    }
    uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);
    QueueFamilyIndices findPhysicalQueueFamilies()
    {
      return findQueueFamilies(physicalDevice);
    }
    VkFormat findSupportedFormat(
      const std::vector<VkFormat> &candidates, VkImageTiling tiling, VkFormatFeatureFlags features);

    // Buffer Helper Functions
    void createBuffer(
      VkDeviceSize size,
      VkBufferUsageFlags usage,
      VkMemoryPropertyFlags properties,
      VkBuffer &buffer,
      VkDeviceMemory &bufferMemory);
    VkCommandBuffer beginSingleTimeCommands();
    void endSingleTimeCommands(VkCommandBuffer commandBuffer);
    void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);
    void copyBufferToImage(
      VkBuffer buffer, VkImage image, uint32_t width, uint32_t height, uint32_t layerCount);

    void createImageWithInfo(
      const VkImageCreateInfo &imageInfo,
      VkMemoryPropertyFlags properties,
      VkImage &image,
      VkDeviceMemory &imageMemory);

    VkPhysicalDeviceProperties properties;

  private:
    void createInstance();
    void setupDebugMessenger();
    void createSurface();
    void pickPhysicalDevice();
    void createLogicalDevice();
    void createCommandPool();

    // helper functions
    bool isDeviceSuitable(VkPhysicalDevice device);
    std::vector<const char *> getRequiredExtensions();
    bool checkValidationLayerSupport();
    QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device);
    void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT &createInfo);
    void hasGflwRequiredInstanceExtensions();
    bool checkDeviceExtensionSupport(VkPhysicalDevice device);
    SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device);

    VkInstance instance;
    VkDebugUtilsMessengerEXT debugMessenger;
    VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
    RveWindow &window;
    VkCommandPool commandPool;

    VkDevice device_;
    VkSurfaceKHR surface_;
    VkQueue graphicsQueue_;
    VkQueue presentQueue_;

    const std::vector<const char *> validationLayers = {"VK_LAYER_KHRONOS_validation"};
    const std::vector<const char *> deviceExtensions = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};
}; // RVE Device ^

} // namespace rve ^

#endif