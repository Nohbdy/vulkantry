#ifndef RVE_PIPELINE_H
#define RVE_PIPELINE_H

#include "rve_device.hpp"

// Standard Libraries
#include <string>
#include <vector>

namespace rve
{

// Pipeline Config Info
struct PipelineConfigInfo
{
  PipelineConfigInfo(const PipelineConfigInfo&) = delete;
  PipelineConfigInfo& operator=(const PipelineConfigInfo&) = delete;
  
  VkViewport viewport;
  VkRect2D scissor;
  
  VkPipelineViewportStateCreateInfo viewportInfo;
  VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo;
  VkPipelineRasterizationStateCreateInfo rasterizationInfo;
  VkPipelineMultisampleStateCreateInfo multisampleInfo;
  VkPipelineColorBlendAttachmentState colorBlendAttachment;
  VkPipelineColorBlendStateCreateInfo colorBlendInfo;
  VkPipelineDepthStencilStateCreateInfo depthStencilInfo;
  //std::vector<VkDynamicState> dynamicStateEnables;
  //VkPipelineDynamicStateCreateInfo dynamicStateInfo;
  VkPipelineLayout pipelineLayout = nullptr;
  VkRenderPass renderPass = nullptr;
  uint32_t subpass = 0;
}; // Pipeline Config Info ^

// Pipeline
class RvePipeline
{
	public:
    RvePipeline(
      RveDevice &device,
      const std::string& vertFilepath,
      const std::string& fragFilepath,
      const PipelineConfigInfo& configInfo);
    ~RvePipeline();
    
    RvePipeline(const RvePipeline&) = delete;
    RvePipeline& operator=(const RvePipeline&) = delete;
    
    void bind(VkCommandBuffer commandBuffer);
    
    static void defaultPipelineConfigInfo(PipelineConfigInfo& configInfo, uint32_t width, uint32_t height);
    
	private:
    static std::vector<char> readFile(const std::string& filepath);
    
    void createGraphicsPipeline(
      const std::string& vertFilepath,
      const std::string& fragFilepath,
      const PipelineConfigInfo& configInfo);
    
    void createShaderModule(const std::vector<char>& code, VkShaderModule* shaderModule);

    RveDevice& rveDevice;
    VkPipeline graphicsPipeline;
    VkShaderModule vertShaderModule;
    VkShaderModule fragShaderModule;
}; // Pipeline ^

} // namespace rve ^

#endif