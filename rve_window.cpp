#include "rve_window.hpp"

// Standard Libraries
#include <stdexcept>

namespace rve
{
  
// Constructor
RveWindow::RveWindow(int w, int h, std::string name) : width{w}, height{h}, windowName{name}
{
  initWindow();
}// Constructor ^

// Destructor
RveWindow::~RveWindow()
{
  glfwDestroyWindow(window);
  glfwTerminate();
}// Destructor ^

// Init Window
void RveWindow::initWindow()
{
  glfwInit();
  glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
  glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
  
  window = glfwCreateWindow(width, height, windowName.c_str(), nullptr, nullptr);
} // Init Window ^

// Create Window Surface
void RveWindow::createWindowSurface(VkInstance instance, VkSurfaceKHR *surface)
{
  if (glfwCreateWindowSurface(instance, window, nullptr, surface) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to create window surface");
  }
} // Create Window Surface ^

}