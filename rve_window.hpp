#ifndef RVE_WINDOW_H
#define RVE_WINDOW_H

// GLFW
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

// Standard Libraries
#include <string>
namespace rve
{

// RVE Window
class RveWindow
{
  public:
    RveWindow(int w, int h, std::string name);
    ~RveWindow();
    
    RveWindow(const RveWindow &) = delete;
    RveWindow &operator=(const RveWindow &) = delete;
    
    bool shouldClose() { return glfwWindowShouldClose(window); }
    VkExtent2D getExtent() { return {static_cast<uint32_t>(width), static_cast<uint32_t>(height)}; }
    
    void createWindowSurface(VkInstance instance, VkSurfaceKHR *surface);
  
  private:
    void initWindow();

    const int width;
    const int height;

    std::string windowName;
    GLFWwindow *window;
}; // RVE Window ^

} // namespace rve ^

#endif